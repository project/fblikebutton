<?php

namespace Drupal\Tests\fblikebutton\Functional;

use Drupal\Core\Cache\Cache;
use Drupal\Tests\node\Functional\NodeTestBase;

/**
 * Class FacebookLikeButtonTest. The base class for testing fblikebutton.
 */
class FacebookLikeButtonTest extends NodeTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['node', 'fblikebutton'];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Test facebook link button.
   */
  public function testFacebookLikeButton() {
    // Log in as an admin user with permission to manage fblikebutton settings.
    $admin = $this->drupalCreateUser([], NULL, TRUE);
    $this->drupalLogin($admin);

    // Create a new node type.
    $this->createContentType(['type' => 'fblikebutton']);

    // Enable fblikebutton element for node entity type.
    $config = \Drupal::configFactory()->getEditable('fblikebutton.settings');
    $config->set('node_types', ['fblikebutton' => 'fblikebutton']);
    $config->save();

    // Clear the cache.
    Cache::invalidateTags(['entity_field_info']);

    /** @var \Drupal\Core\Entity\EntityDisplayRepository $display_repository */
    $display_repository = \Drupal::service('entity_display.repository');

    // Add fblikebutton links to the output.
    $display_repository->getViewDisplay('node', 'fblikebutton', 'default')
      ->setComponent('fblikebutton')
      ->save();

    // Check the status of the page.
    $this->drupalGet('node/add/fblikebutton');
    $this->assertSession()->statusCodeEquals(200);

    $edit = [];
    $title = $this->randomMachineName(8);
    $edit['title[0][value]'] = $title;
    $this->drupalGet('node/add/fblikebutton');
    $this->submitForm($edit, 'Save');

    $node = $this->drupalGetNodeByTitle($title);

    // Check if the output contains required strings.
    $this->drupalGet("node/{$node->id()}");
    $this->assertSession()->pageTextContains($title);
    $this->assertSession()->elementExists('css', '.fb-like');

    // Remove the component from the page.
    $display_repository->getViewDisplay('node', 'fblikebutton', 'default')
      ->removeComponent('fblikebutton')
      ->save();

    // Check if the output contains required strings.
    $this->drupalGet("node/{$node->id()}");
    $this->assertSession()->pageTextContains($title);
    $this->assertSession()->elementNotExists('css', '.fb-like');
  }

}
