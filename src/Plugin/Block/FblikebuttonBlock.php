<?php

namespace Drupal\fblikebutton\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Path\PathMatcherInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a Facebook Like Button Block.
 *
 * @Block(
 *   id = "fblikebutton_block",
 *   admin_label = @Translation("Facebook Like Button"),
 * )
 */
class FblikebuttonBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The path matcher service.
   *
   * @var \Drupal\Core\Path\PathMatcherInterface
   */
  protected $pathMatcher;

  /**
   * Constructs a FblikebuttonBlockobject.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Path\PathMatcherInterface $path_matcher
   *   Provides an interface for URL path matchers.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, PathMatcherInterface $path_matcher) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->pathMatcher = $path_matcher;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('path.matcher')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    global $base_url;
    return [
      'block_url' => $base_url,
      'layout' => 'standard',
      'size' => 'small',
      'action' => 'like',
      'colorscheme' => 'light',
      'language' => 'en_US',
      'width' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);
    $config = $this->getConfiguration();

    $form['settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Button settings'),
      '#open' => TRUE,
    ];
    $form['settings']['block_url'] = [
      '#type' => 'textfield',
      '#default_value' => $config['block_url'],
      '#description' => $this->t('URL of the page to like (could be your homepage or a facebook page e.g.)<br> You can also specify &lt;current&gt; to establish the url for the current viewed page in your site'),
    ];
    $form['appearance'] = [
      '#type' => 'details',
      '#title' => $this->t('Button appearance'),
      '#open' => FALSE,
    ];
    $form['appearance']['layout'] = [
      '#type' => 'select',
      '#title' => $this->t('Layout style'),
      '#options' => [
        'standard' => $this->t('Standard'),
        'box_count' => $this->t('Box Count'),
        'button_count' => $this->t('Button Count'),
        'button' => $this->t('Button'),
      ],
      '#default_value' => $config['layout'],
      '#description' => $this->t('Determines the size and amount of social context next to the button'),
    ];
    $form['appearance']['size'] = [
      '#type' => 'select',
      '#title' => $this->t('Size'),
      '#options' => ['small' => $this->t('Small'), 'large' => $this->t('Large')],
      '#default_value' => $config['size'],
      '#description' => $this->t('The button is offered in 2 sizes i.e. large and small (default).'),
    ];
    $form['appearance']['action'] = [
      '#type' => 'select',
      '#title' => $this->t('Verb to display'),
      '#options' => ['like' => $this->t('Like'), 'recommend' => $this->t('Recommend')],
      '#default_value' => $config['action'],
      '#description' => $this->t('The verb to display in the button.'),
    ];
    $form['appearance']['colorscheme'] = [
      '#type' => 'select',
      '#title' => $this->t('Color scheme'),
      '#options' => ['light' => $this->t('Light'), 'dark' => $this->t('Dark')],
      '#default_value' => $config['colorscheme'],
      '#description' => $this->t('The color scheme of box environment'),
    ];
    $form['appearance']['language'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Language'),
      '#default_value' => $config['language'],
      '#description' => $this->t('Specific language to use. Default is English. Examples:<br />French (France): <em>fr_FR</em><br />French (Canada): <em>fr_CA</em><br />More information can be found at http://developers.facebook.com/docs/internationalization/ and a full XML list can be found at http://www.facebook.com/translations/FacebookLocales.xml'),
    ];
    $form['appearance']['width'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Width'),
      '#default_value' => $config['width'],
      '#description' => $this->t('The width of the plugin (standard layout only), which is subject to the minimum and default width.'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $values = $form_state->getValues();

    $this->configuration['layout'] = $values['appearance']['layout'];
    $this->configuration['size'] = $values['appearance']['size'];
    $this->configuration['block_url'] = $values['settings']['block_url'];
    $this->configuration['action'] = $values['appearance']['action'];
    $this->configuration['colorscheme'] = $values['appearance']['colorscheme'];
    $this->configuration['language'] = $values['appearance']['language'];
    $this->configuration['width'] = $values['appearance']['width'];
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $block = [
      '#theme' => 'fblikebutton',
      '#layout' => $this->configuration['layout'],
      '#size' => $this->configuration['size'],
      '#action' => $this->configuration['action'],
      '#colorscheme' => $this->configuration['colorscheme'],
      '#language' => $this->configuration['language'],
      '#width' => $this->configuration['width'],
    ];

    // If it's not for the current page.
    if ($this->configuration['block_url'] != '<current>') {
      $block['#url'] = $this->configuration['block_url'];
    }
    else {
      // Avoid this block to be cached.
      $block['#cache'] = [
        'max-age' => 0,
      ];

      // Drupal uses the /node path to refers to the frontpage. That's why
      // facebook could point to www.example.com/node instead of
      // wwww.example.com.
      // To avoid this, we check if the current path is the frontpage
      // Check if the current path is the front page.
      if ($this->pathMatcher->isFrontPage()) {
        global $base_url;
        $block['#url'] = $base_url;
      }
      else {
        $block['#url'] = Url::fromRoute('<current>', [], ['absolute' => TRUE])->toString();
      }
    }

    return $block;
  }

}
