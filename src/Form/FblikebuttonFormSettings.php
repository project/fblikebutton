<?php

namespace Drupal\fblikebutton\Form;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Provides setting form for fblikebutton module.
 */
class FblikebuttonFormSettings extends ConfigFormBase {
  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'fblikebutton_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function getEditableConfigNames() {
    return [
      'fblikebutton.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $fblikebutton_node_options = node_type_get_names();
    $config = $this->config('fblikebutton.settings');

    $form['fblikebutton_dynamic_visibility'] = [
      '#type' => 'details',
      '#title' => $this->t('Visibility settings'),
      '#open' => TRUE,
    ];
    $form['fblikebutton_dynamic_visibility']['fblikebutton_node_types'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Display the Like button on these content types:'),
      '#options' => $fblikebutton_node_options,
      '#default_value' => $config->get('node_types'),
      '#description' => $this->t('Each of these content types will have the "like" button automatically added to them.'),
    ];
    $form['fblikebutton_dynamic_appearance'] = [
      '#type' => 'details',
      '#title' => $this->t('Appearance settings'),
      '#open' => TRUE,
    ];
    $form['fblikebutton_dynamic_appearance']['fblikebutton_layout'] = [
      '#type' => 'select',
      '#title' => $this->t('Layout style'),
      '#options' => [
        'standard' => $this->t('Standard'),
        'box_count' => $this->t('Box Count'),
        'button_count' => $this->t('Button Count'),
        'button' => $this->t('Button'),
      ],
      '#default_value' => $config->get('layout'),
      '#description' => $this->t('Determines the size and amount of social context next to the button.'),
    ];

    $form['fblikebutton_dynamic_appearance']['fblikebutton_size'] = [
      '#type' => 'select',
      '#title' => $this->t('Size'),
      '#options' => ['small' => $this->t('Small'), 'large' => $this->t('Large')],
      '#default_value' => $config->get('size'),
      '#description' => $this->t('The button is offered in 2 sizes i.e. large and small (default).'),
    ];
    $form['fblikebutton_dynamic_appearance']['fblikebutton_action'] = [
      '#type' => 'select',
      '#title' => $this->t('Verb to display'),
      '#options' => ['like' => $this->t('Like'), 'recommend' => $this->t('Recommend')],
      '#default_value' => $config->get('action'),
      '#description' => $this->t('The verbiage to display inside the button itself.'),
    ];
    $form['fblikebutton_dynamic_appearance']['fblikebutton_colorscheme'] = [
      '#type' => 'select',
      '#title' => $this->t('Color scheme'),
      '#options' => ['light' => $this->t('Light'), 'dark' => $this->t('Dark')],
      '#default_value' => $config->get('colorscheme'),
      '#description' => $this->t('The color scheme of the box environment.'),
    ];

    $language_description = "Specific language to use. Default is English. Examples:<br />French (France): <em>fr_FR</em><br />";
    $language_description .= "French (Canada): <em>fr_CA</em><br />More information can be found at <a href='@info_url'>@info_url</a> ";
    $language_description .= "and a full XML list can be found at <a href='@list_url'>@list_url</a>";
    $form['fblikebutton_dynamic_appearance']['fblikebutton_language'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Language'),
      '#default_value' => $config->get('language'),
      '#description' => $this->getStringTranslation()->translate($language_description, [
        '@info_url' => 'http://developers.facebook.com/docs/internationalization',
        '@list_url' => 'http://www.facebook.com/translations/FacebookLocales.xml',
      ]),
    ];

    $form['fblikebutton_dynamic_appearance']['fblikebutton_width'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Width'),
      '#default_value' => $config->get('width'),
      '#description' => $this->t('The width of the plugin (standard layout only), which is subject to the minimum and default width.'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $config = $this->config('fblikebutton.settings');

    $config->set('node_types', $form_state->getValue('fblikebutton_node_types'))
      ->set('layout', $form_state->getValue('fblikebutton_layout'))
      ->set('size', $form_state->getValue('fblikebutton_size'))
      ->set('action', $form_state->getValue('fblikebutton_action'))
      ->set('colorscheme', $form_state->getValue('fblikebutton_colorscheme'))
      ->set('language', $form_state->getValue('fblikebutton_language'))
      ->set('width', $form_state->getValue('fblikebutton_width'))
      ->save();

    Cache::invalidateTags(['entity_field_info']);
  }

}
