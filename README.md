# Facebook Like Button

Rather than having to manually copy/paste the "Like this on Facebook" code for
each piece of content you (or your users) create, this module will automatically
add that code to the end of each chosen node type. You are able to add a Like
button which likes a given URL (static, e.g. your homepage) or/and put a dynamic
Like button on your page to like the page you are actually visiting.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/fblikebutton).

To submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/fblikebutton).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

Go to "Configuration" -> "User Interface" -> "Facebook Like Button Settings" to
find all the configuration options.


## Maintainers

- Getulio Valentin Sánchez - [gvso](https://www.drupal.org/u/gvso)
- Jeremy Trojan - [jerdiggity](https://www.drupal.org/u/jerdiggity)
- lliss - [lliss](https://www.drupal.org/u/lliss)
- Pavlo Samchuk - [id.medion](https://www.drupal.org/u/idmedion)
- Viktor Holovachek - [AstonVictor](https://www.drupal.org/u/astonvictor)
